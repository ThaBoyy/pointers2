#include <iostream>
using namespace std;

int main()
{
    int x = 25, y = 2 * x;
    auto a = &x, b = a;
    auto c = *a, d = 2 * *b;

    // cout << "x = " << x << ", y = " << y << endl;

    // print All The Values Of A B C D X Y And Their Addresses
    cout << endl;
    cout << "Value Of a : " << *a << "       And The Address Is : " << &a << endl;
    cout << "Value Of b : " << *b << "       And The Address Is : " << &b << endl;
    cout << "Value Of c : " << c << "       And The Address Is : " << &c << endl;
    cout << "Value Of d : " << d << "       And The Address Is : " << &d << endl;
    cout << "Value Of x : " << x << "       And The Address Is : " << &x << endl;
    cout << "Value Of y : " << y << "       And The Address Is : " << &y << endl;
    cout << endl << "I Am A Pointers Boss!" << endl << endl;
    return 0;
}
